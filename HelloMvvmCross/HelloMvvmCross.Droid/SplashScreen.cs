using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace HelloMvvmCross.Droid
{
    [Activity(MainLauncher = true, 
        NoHistory = true,
        Theme = "@style/Theme.Splash",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen() : base(Resource.Layout.SplashScreen)
        {
        }
    }
}
