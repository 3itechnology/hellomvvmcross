using Acr.UserDialogs;
using Android.App;
using Android.OS;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views;

namespace HelloMvvmCross.Droid.Views
{
    public class Login : MvxFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.login, null);

            UserDialogs.Init(this.Activity);

            return view;
        }
    }
}
