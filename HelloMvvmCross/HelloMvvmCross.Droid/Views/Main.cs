using Android.App;
using Android.OS;
using HelloMvvmCross.ViewModels;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;

namespace HelloMvvmCross.Droid.Views
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Main : MvxFragmentActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.main);

            // set custom presenter
            var presenter = (DroidPresenter)Mvx.Resolve<IMvxAndroidViewPresenter>();
            var initialFragment = new Login { ViewModel = Mvx.IocConstruct<LoginViewModel>() };

            presenter.RegisterFragmentManager(SupportFragmentManager, initialFragment);
        }
    }
}
