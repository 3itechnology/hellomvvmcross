using System;
using MvvmCross.Core.ViewModels;

namespace HelloMvvmCross.ViewModels
{
    public class Login2ViewModel : MvxViewModel
    {
        public Login2ViewModel()
        {
        }

        private string _text = "Hello MvvmCross";
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }
    }
}