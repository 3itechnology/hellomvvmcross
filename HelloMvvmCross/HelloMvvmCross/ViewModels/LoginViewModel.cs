using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Acr.UserDialogs;

namespace HelloMvvmCross.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        public LoginViewModel()
        {
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public IMvxCommand LoginCommand => new MvxCommand(DoLogin);
        private void DoLogin()
        {
            if (string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password))
            {
                Mvx.Resolve<IUserDialogs>().Alert("User name and password required");
            }
            else
            {
                // auth logic

                bool didAuthFail = true;

                if (didAuthFail)
                {
                    Mvx.Resolve<IUserDialogs>().Alert("Authentication Error");
                }
                else
                {
                    ShowViewModel<Login2ViewModel>();
                }
            }
        }
    }
}