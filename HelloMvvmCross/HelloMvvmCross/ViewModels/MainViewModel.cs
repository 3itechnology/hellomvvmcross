using System;
using MvvmCross.Core.ViewModels;

namespace HelloMvvmCross.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        public MainViewModel()
        {
        }
        
        public IMvxCommand ChangeTextCommand => new MvxCommand(ChangeText);
        private void ChangeText()
        {
            var rando = new Random();
            Text = "Hello MvvmCross " + rando.Next(0, 1000);
        }

        private string _text = "Hello MvvmCross";
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }
    }
}